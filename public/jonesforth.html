<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>JonesForth</title>
<style>
html { font-size: 16px; font-family: serif; }
code, pre { font-size: 14px; font-family: monospace; }
body { max-width: 60em; margin: 0; padding: 1em 1em 4em; }
pre, blockquote {  padding: 1em; background-color: #f5f5f5; }
blockquote { margin: 0; }
pre { overflow-x: auto; }
td { padding-left: 1em; }
.hide { display: none; }
.include { border: 1px solid black; }
h1 { font-size: 150%; } h2 { font-size: 125%; } h3 { font-size: 110%; }
img { max-width: 100%; }
</style>
</head>
<body>
<p>This is a modified <a href="https://github.com/nornagon/jonesforth/">JonesForth tutorial</a>
for viewing in the browser. URL formatted as html link. Dead link is fixed.
Diagram is redrawn as png image. Also added table of contents.</p>
<hr>
<h1>Table of Contents</h1>
<ul>
  <li><a href="#jonesforth">JonesForth</a></li>
  <li><a href="#introduction">Introduction</a></li>
  <li><a href="#acknowledgements">Acknowledgments</a></li>
  <li><a href="#publicdomain">Public Domain</a></li>
  <li><a href="#convention">Convention</a></li>
  <li><a href="#assembling">Assembling</a></li>
  <li><a href="#assembler">Assembler</a></li>
  <li><a href="#thedictionary">The Dictionary</a></li>
  <li><a href="#directthreadedcode">Direct Threaded Code</a></li>
  <li><a href="#indirectthreadedcode">Indirect Threaded Code</a></li>
</ul>
<hr>
<h1 id="jonesforth">JonesForth</h1>
<p>A sometimes minimal FORTH compiler and tutorial for Linux / i386 systems.</p>
<p>By Richard W.M. Jones &lt;rich@annexia.org&gt;
<a href="https://web.archive.org/web/20090209211708/http://www.annexia.org:80/forth">http://annexia.org/forth</a></p>
<p>This is PUBLIC DOMAIN (see public domain release statement below).</p>
<p>$Id: jonesforth.S,v 1.47 2009-09-11 08:33:13 rich Exp $</p>
<pre>gcc -m32 -nostdlib -static -Wl,-Ttext,0 -Wl,--build-id=none -o jonesforth jonesforth.S</pre>

<p><a id="segment-01" href="#segment-01">#segment-01</a>
<pre class="include">.set JONES_VERSION,47</pre>
</p>

<h2 id="introduction">Introduction</h2>
<p>FORTH is one of those alien languages which most working programmers regard in the same
way as Haskell, LISP, and so on.  Something so strange that they'd rather any thoughts
of it just go away so they can get on with writing this paying code.  But that's wrong
and if you care at all about programming then you should at least understand all these
languages, even if you will never use them.</p>

<p>LISP is the ultimate high-level language, and features from LISP are being added every
decade to the more common languages.  But FORTH is in some ways the ultimate in low level
programming.  Out of the box it lacks features like dynamic memory management and even
strings.  In fact, at its primitive level it lacks even basic concepts like IF-statements
and loops.</p>

<p>Why then would you want to learn FORTH?  There are several very good reasons.  First
and foremost, FORTH is minimal.  You really can write a complete FORTH in, say, 2000
lines of code.  I don't just mean a FORTH program, I mean a complete FORTH operating
system, environment and language.  You could boot such a FORTH on a bare PC and it would
come up with a prompt where you could start doing useful work.  The FORTH you have here
isn't minimal and uses a Linux process as its 'base PC' (both for the purposes of making
it a good tutorial). It's possible to completely understand the system.  Who can say they
completely understand how Linux works, or gcc?</p>

<p>Secondly FORTH has a peculiar bootstrapping property.  By that I mean that after writing
a little bit of assembly to talk to the hardware and implement a few primitives, all the
rest of the language and compiler is written in FORTH itself.  Remember I said before
that FORTH lacked IF-statements and loops?  Well of course it doesn't really because
such a lanuage would be useless, but my point was rather that IF-statements and loops are
written in FORTH itself.</p>

<p>Now of course this is common in other languages as well, and in those languages we call
them 'libraries'.  For example in C, 'printf' is a library function written in C.  But
in FORTH this goes way beyond mere libraries.  Can you imagine writing C's 'if' in C?
And that brings me to my third reason: If you can write 'if' in FORTH, then why restrict
yourself to the usual if/while/for/switch constructs?  You want a construct that iterates
over every other element in a list of numbers?  You can add it to the language.  What
about an operator which pulls in variables directly from a configuration file and makes
them available as FORTH variables?  Or how about adding Makefile-like dependencies to
the language?  No problem in FORTH.  How about modifying the FORTH compiler to allow
complex inlining strategies &mdash; simple.  This concept isn't common in programming languages,
but it has a name (in fact two names): "macros" (by which I mean LISP-style macros, not
the lame C preprocessor) and "domain specific languages" (DSLs).</p>

<p>This tutorial isn't about learning FORTH as the language.  I'll point you to some references
you should read if you're not familiar with using FORTH.  This tutorial is about how to
write FORTH.  In fact, until you understand how FORTH is written, you'll have only a very
superficial understanding of how to use it.</p>

<p>So if you're not familiar with FORTH or want to refresh your memory here are some online
references to read:</p>
<ul>
  <li><a href="http://en.wikipedia.org/wiki/Forth_%28programming_language%29">Forth's Wikipedia Page</a></li>
  <li><a href="http://galileo.phys.virginia.edu/classes/551.jvn.fall01/primer.htm">A Beginner's Guide to Forth by J.V. Noble</a></li>
  <li><a href="http://wiki.laptop.org/go/Forth_Lessons">OLPC Wiki - Forth Lesson</a></li>
  <li><a href="https://web.archive.org/web/20080612234512/http://www.albany.net:80/~hello/simple.htm">Simple Forth</a></li>
  <li><a href="https://bernd-paysan.de/why-forth.html">Another "Why FORTH?" essay</a></li>
  <!--
  <li>above is new link based on 301 redirect of the original <a href="http://www.jwdt.com/~paysan/why-forth.html">Why Forth (dead)</a></li>
  -->
  <li><a href="http://lambda-the-ultimate.org/node/2452">Discussion and criticism of this FORTH</a></li>
</ul>


<h2 id="acknowledgements">Acknowledgments</h2>
<p>This code draws heavily on the design of <a href="http://home.hccnet.nl/a.w.m.van.der.horst/lina.html">LINA FORTH</a>
by Albert van der Horst.  Any similarities in the code are probably not accidental.</p>

<p>Some parts of this FORTH are also based on <a href="http://ftp.funet.fi/pub/doc/IOCCC/1992/buzzard.2.design">this IOCCC entry from 1992</a>.
I was very proud when Sean Barrett, the original author of the IOCCC entry,
<a href="http://lambda-the-ultimate.org/node/2452#comment-36818">commented in the LtU thread about this FORTH</a>.</p>

<p>And finally I'd like to acknowledge the (possibly forgotten?) authors of <a href="http://en.wikipedia.org/wiki/Artic_Software">ARTIC FORTH</a> because their
original program which I still have on original cassette tape kept nagging away at me all these years.</p>


<h2 id="publicdomain">Public Domain</h2>
<blockquote>
<p>I, the copyright holder of this work, hereby release it into the public domain. This applies worldwide.</p>

<p>In case this is not legally possible, I grant any entity the right to use this work for any purpose,
without any conditions, unless such conditions are required by law.</p>
</blockquote>
<h2 id="convention">Convention</h2>

<p>This tutorial was translated from assembly source into HTML. The original tutorial text is inside the
comment section of the source code. In this version, the non-comment part is the one inside code block
with black border, see the declaration of JONES_VERSION above as an example.</p>

<p class="hide">
<![CDATA[
	SETTING UP ----------------------------------------------------------------------

	Let's get a few housekeeping things out of the way.  Firstly because I need to draw lots of
	ASCII-art diagrams to explain concepts, the best way to look at this is using a window which
	uses a fixed width font and is at least this wide:

 <------------------------------------------------------------------------------------------------------------------------>

	Secondly make sure TABS are set to 8 characters.  The following should be a vertical
	line.  If not, sort out your tabs.

		|
	        |
	    	|

	Thirdly I assume that your screen is at least 50 characters high.
]]>
</p>


<h2 id="assembling">Assembling</h2>

<p>If you want to actually run this FORTH, rather than just read it, you will need Linux on an
i386.  Linux because instead of programming directly to the hardware on a bare PC which I
could have done, I went for a simpler tutorial by assuming that the 'hardware' is a Linux
process with a few basic system calls (read, write and exit and that's about all).  i386
is needed because I had to write the assembly for a processor, and i386 is by far the most
common.  (Of course when I say 'i386', any 32- or 64-bit x86 processor will do.  I'm compiling
this on a 64 bit AMD Opteron).</p>

<p>Again, to assemble this you will need gcc and gas (the GNU assembler).  The commands to
assemble and run the code (save this file as 'jonesforth.S') are:</p>

<pre>
gcc -m32 -nostdlib -static -Wl,-Ttext,0 -Wl,--build-id=none -o jonesforth jonesforth.S
cat jonesforth.f - | ./jonesforth
</pre>

<p>If you want to run your own FORTH programs you can do:</p>

<pre>
cat jonesforth.f myprog.f | ./jonesforth
</pre>

<p>If you want to load your own FORTH code and then continue reading user commands, you can do:

<pre>
cat jonesforth.f myfunctions.f - | ./jonesforth
</pre>


<h2 id="assembler">Assembler</h2>

<p>(You can just skip to the next section &mdash; you don't need to be able to read assembler to
follow this tutorial).</p>

<p>However if you do want to read the assembly code here are a few notes about gas (the GNU assembler):</p>

<ol>
  <li>
    <p>Register names are prefixed with '<code>%</code>', so <code>%eax</code> is the 32 bit i386 accumulator.
    The registers available on i386 are: <code>%eax</code>, <code>%ebx</code>, <code>%ecx</code>, <code>%edx</code>,
    <code>%esi</code>, <code>%edi</code>, <code>%ebp</code> and <code>%esp</code>, and most of them
    have special purposes.</p>
  </li>
  <li>
    <p><code>add</code>, <code>mov</code>, etc. take arguments in the form <code>SRC,DEST</code>.
    So <code>mov %eax,%ecx</code> moves <code>%eax &rarr; %ecx</code></p>
  </li>
  <li>
    <p>Constants are prefixed with '<code>$</code>', and you mustn't forget it!  If you forget it then it
    causes a read from memory instead, so:</p>
    <table>
      <tr>
	<td><code>mov $2,%eax</code></td>
	<td>moves number 2 into <code>%eax</code></td>
      </tr>
      <tr>
	<td><code>mov 2,%eax</code></td>
	<td>reads the 32 bit word from address 2 into <code>%eax</code> (ie. most likely a mistake)</td>
      </tr>
  </table>
  </li>
  <li>
    <p>gas has a funky syntax for local labels, where '<code>1f</code>' (etc.) means label '<code>1:</code>' "forwards"
    and '<code>1b</code>' (etc.) means label '<code>1:</code>' "backwards".  Notice that these labels might be mistaken
    for hex numbers (eg. you might confuse <code>1b</code> with <code>$0x1b</code>).</p>
  </li>
  <li>
    <p>'<code>ja</code>' is "jump if above", '<code>jb</code>' for "jump if below", '<code>je</code>' "jump if equal" etc.</p>
  </li>
  <li>
    <p>gas has a reasonably nice <code>.macro</code> syntax, and I use them a lot to make the code shorter and
    less repetitive.</p>
  </li>
</ol>

<p>For more help reading the assembler, do "<code>info gas</code>" at the Linux prompt.</p>

<p>Now the tutorial starts in earnest.</p>

<h2 id="thedictionary">The Dictionary</h2>

<p>In FORTH as you will know, functions are called "words", and just as in other languages they
have a name and a definition.  Here are two FORTH words:</p>

<pre>
: DOUBLE DUP + ;		\ name is "DOUBLE", definition is "DUP +"
: QUADRUPLE DOUBLE DOUBLE ;	\ name is "QUADRUPLE", definition is "DOUBLE DOUBLE"
</pre>

<p>Words, both built-in ones and ones which the programmer defines later, are stored in a dictionary
which is just a linked list of dictionary entries.</p>

<p><a href="images/jonesforth-fig1.png"><img src="images/jonesforth-fig1.png"/></a></p>

<p class="hide">
<![CDATA[
	<--- DICTIONARY ENTRY (HEADER) ----------------------->
	+------------------------+--------+---------- - - - - +----------- - - - -
	| LINK POINTER           | LENGTH/| NAME	      | DEFINITION
	|			 | FLAGS  |     	      |
	+--- (4 bytes) ----------+- byte -+- n bytes  - - - - +----------- - - - -
]]>
</p>

<p>I'll come to the definition of the word later.  For now just look at the header.  The first
4 bytes are the link pointer.  This points back to the previous word in the dictionary, or, for
the first word in the dictionary it is just a NULL pointer.  Then comes a length/flags byte.
The length of the word can be up to 31 characters (5 bits used) and the top three bits are used
for various flags which I'll come to later.  This is followed by the name itself, and in this
implementation the name is rounded up to a multiple of 4 bytes by padding it with zero bytes.
That's just to ensure that the definition starts on a 32 bit boundary.</p>

<p>A FORTH variable called LATEST contains a pointer to the most recently defined word, in
other words, the head of this linked list.</p>

<p>DOUBLE and QUADRUPLE might look like this:</p>

<p><a href="images/jonesforth-fig2.png"><img src="images/jonesforth-fig2.png" /></a></p>
<pre class="hide">
<![CDATA[
	  pointer to previous word
	   ^
	   |
	+--|------+---+---+---+---+---+---+---+---+------------- - - - -
	| LINK    | 6 | D | O | U | B | L | E | 0 | (definition ...)
	+---------+---+---+---+---+---+---+---+---+------------- - - - -
           ^       len                         padding
	   |
	+--|------+---+---+---+---+---+---+---+---+---+---+---+---+------------- - - - -
	| LINK    | 9 | Q | U | A | D | R | U | P | L | E | 0 | 0 | (definition ...)
	+---------+---+---+---+---+---+---+---+---+---+---+---+---+------------- - - - -
           ^       len                                     padding
           |
           |
	  LATEST
]]>
</pre>
<p>You should be able to see from this how you might implement functions to find a word in
the dictionary (just walk along the dictionary entries starting at LATEST and matching
the names until you either find a match or hit the NULL pointer at the end of the dictionary);
and add a word to the dictionary (create a new definition, set its LINK to LATEST, and set
LATEST to point to the new word).  We'll see precisely these functions implemented in
assembly code later on.</p>

<p>One interesting consequence of using a linked list is that you can redefine words, and
a newer definition of a word overrides an older one.  This is an important concept in
FORTH because it means that any word (even "built-in" or "standard" words) can be
overridden with a new definition, either to enhance it, to make it faster or even to
disable it.  However because of the way that FORTH words get compiled, which you'll
understand below, words defined using the old definition of a word continue to use
the old definition.  Only words defined after the new definition use the new definition.</p>


<h2 id="directthreadedcode">Direct Threaded Code</h2>

<p>Now we'll get to the really crucial bit in understanding FORTH, so go and get a cup of tea
or coffee and settle down.  It's fair to say that if you don't understand this section, then you
won't "get" how FORTH works, and that would be a failure on my part for not explaining it well.
So if after reading this section a few times you don't understand it, please email me
(rich@annexia.org).</p>

<p>Let's talk first about what "threaded code" means.  Imagine a peculiar version of C where
you are only allowed to call functions without arguments.  (Don't worry for now that such a
language would be completely useless!)  So in our peculiar C, code would look like this:</p>

<pre>
f ()
{
  a ();
  b ();
  c ();
}
</pre>

<p>and so on.  How would a function, say 'f' above, be compiled by a standard C compiler?
Probably into assembly code like this.  On the right hand side I've written the actual
i386 machine code.</p>

<pre>
f:
  CALL a			E8 08 00 00 00
  CALL b			E8 1C 00 00 00
  CALL c			E8 2C 00 00 00
  ; ignore the return from the function for now
</pre>

<p>"E8" is the x86 machine code to "CALL" a function.  In the first 20 years of computing
memory was hideously expensive and we might have worried about the wasted space being used
by the repeated "E8" bytes.  We can save 20% in code size (and therefore, in expensive memory)
by compressing this into just:</p>

<pre>
08 00 00 00		Just the function addresses, without
1C 00 00 00		the CALL prefix.
2C 00 00 00
</pre>

<p>On a 16-bit machine like the ones which originally ran FORTH the savings are even greater - 33%.</p>

<p>[Historical note: If the execution model that FORTH uses looks strange from the following
paragraphs, then it was motivated entirely by the need to save memory on early computers.
This code compression isn't so important now when our machines have more memory in their L1
caches than those early computers had in total, but the execution model still has some
useful properties].</p>

<p>Of course this code won't run directly on the CPU any more.  Instead we need to write an
interpreter which takes each set of bytes and calls it.</p>

<p>On an i386 machine it turns out that we can write this interpreter rather easily, in just
two assembly instructions which turn into just 3 bytes of machine code.  Let's store the
pointer to the next word to execute in the %esi register:</p>

<pre>
	08 00 00 00	<- We're executing this one now.  %esi is the _next_ one to execute.
%esi -> 1C 00 00 00
	2C 00 00 00
</pre>

<p>The all-important i386 instruction is called LODSL (or in Intel manuals, LODSW).  It does
two things.  Firstly it reads the memory at %esi into the accumulator (%eax).  Secondly it
increments %esi by 4 bytes.  So after LODSL, the situation now looks like this:</p>

<pre>
	08 00 00 00	<- We're still executing this one
	1C 00 00 00	<- %eax now contains this address (0x0000001C)
%esi -> 2C 00 00 00
</pre>

<p>Now we just need to jump to the address in %eax.  This is again just a single x86 instruction
written JMP *(%eax).  And after doing the jump, the situation looks like:</p>

<pre>
	08 00 00 00
	1C 00 00 00	<- Now we're executing this subroutine.
%esi -> 2C 00 00 00
</pre>

<p>To make this work, each subroutine is followed by the two instructions 'LODSL; JMP *(%eax)'
which literally make the jump to the next subroutine.</p>

<p>And that brings us to our first piece of actual code!  Well, it's a macro.</p>

<p><a id="segment-02" href="#segment-02">#segment-02</a>
<pre class="include">
/* NEXT macro. */
	.macro NEXT
	lodsl
	jmp *(%eax)
	.endm
</pre>

<p>The macro is called <code>NEXT</code>.  That's a FORTH-ism.  It expands to those two instructions.</p>

<p>Every FORTH primitive that we write has to be ended by <code>NEXT</code>.  Think of it kind of like
a return.</p>

<p>The above describes what is known as direct threaded code.</p>

<p>To sum up: We compress our function calls down to a list of addresses and use a somewhat
magical macro to act as a "jump to next function in the list".  We also use one register (<code>%esi</code>)
to act as a kind of instruction pointer, pointing to the next function in the list.</p>

<p>I'll just give you a hint of what is to come by saying that a FORTH definition such as:</p>

<pre>
: QUADRUPLE DOUBLE DOUBLE ;
</pre>

<p>actually compiles (almost, not precisely but we'll see why in a moment) to a list of
function addresses for <code>DOUBLE</code>, <code>DOUBLE</code> and a special function called <code>EXIT</code> to finish off.</p>

<p>At this point, REALLY EAGLE-EYED ASSEMBLY EXPERTS are saying "JONES, YOU'VE MADE A MISTAKE!".</p>

<p>I lied about <code>JMP *(%eax)</code>.</p>


<h2 id="indirectthreadedcode">Indirect Threaded Code</h2>

<p>It turns out that direct threaded code is interesting but only if you want to just execute
a list of functions written in assembly language.  So QUADRUPLE would work only if DOUBLE
was an assembly language function.  In the direct threaded code, QUADRUPLE would look like:</p>

<pre>
	+------------------+
	| addr of DOUBLE  --------------------> (assembly code to do the double)
	+------------------+                    NEXT
%esi ->	| addr of DOUBLE   |
	+------------------+
</pre>

<p>We can add an extra indirection to allow us to run both words written in assembly language
(primitives written for speed) and words written in FORTH themselves as lists of addresses.</p>

<p>The extra indirection is the reason for the brackets in JMP *(%eax).</p>

<p>Let's have a look at how QUADRUPLE and DOUBLE really look in FORTH:</p>

<pre>
: QUADRUPLE DOUBLE DOUBLE ;

+------------------+
| codeword         |		   : DOUBLE DUP + ;
+------------------+
| addr of DOUBLE  ---------------> +------------------+
+------------------+               | codeword         |
| addr of DOUBLE   |		   +------------------+
+------------------+	   	   | addr of DUP   --------------> +------------------+
| addr of EXIT	   |		   +------------------+            | codeword      -------+
+------------------+	   %esi -> | addr of +     --------+	   +------------------+   |
				   +------------------+	   |	   | assembly to    <-----+
				   | addr of EXIT     |    |       | implement DUP    |
				   +------------------+	   |	   |	..	      |
							   |	   |    ..            |
							   |	   | NEXT             |
							   |	   +------------------+
							   |
							   +-----> +------------------+
								   | codeword      -------+
								   +------------------+   |
								   | assembly to   <------+
								   | implement +      |
								   | 	..            |
								   | 	..            |
								   | NEXT      	      |
								   +------------------+
</pre>

<p>This is the part where you may need an extra cup of tea/coffee/favourite caffeinated
beverage.  What has changed is that I've added an extra pointer to the beginning of
the definitions.  In FORTH this is sometimes called the "codeword".  The codeword is
a pointer to the interpreter to run the function.  For primitives written in
assembly language, the "interpreter" just points to the actual assembly code itself.
They don't need interpreting, they just run.</p>

<p>In words written in FORTH (like QUADRUPLE and DOUBLE), the codeword points to an interpreter
function.</p>

<p>I'll show you the interpreter function shortly, but let's recall our indirect
JMP *(%eax) with the "extra" brackets.  Take the case where we're executing DOUBLE
as shown, and DUP has been called.  Note that %esi is pointing to the address of +</p>

<p>The assembly code for DUP eventually does a NEXT.  That:</p>

<pre>
(1) reads the address of + into %eax		%eax points to the codeword of +
(2) increments %esi by 4
(3) jumps to the indirect %eax			jumps to the address in the codeword of +,
						ie. the assembly code to implement +

	+------------------+
	| codeword         |
	+------------------+
	| addr of DOUBLE  ---------------> +------------------+
	+------------------+               | codeword         |
	| addr of DOUBLE   |		   +------------------+
	+------------------+	   	   | addr of DUP   --------------> +------------------+
	| addr of EXIT	   |		   +------------------+            | codeword      -------+
	+------------------+	   	   | addr of +     --------+	   +------------------+   |
					   +------------------+	   |	   | assembly to    <-----+
				   %esi -> | addr of EXIT     |    |       | implement DUP    |
					   +------------------+	   |	   |	..	      |
								   |	   |    ..            |
								   |	   | NEXT             |
								   |	   +------------------+
								   |
								   +-----> +------------------+
									   | codeword      -------+
									   +------------------+   |
								now we're  | assembly to    <-----+
								executing  | implement +      |
								this	   | 	..            |
								function   | 	..            |
									   | NEXT      	      |
									   +------------------+
</pre>

<p>So I hope that I've convinced you that NEXT does roughly what you'd expect.  This is
indirect threaded code.</p>

<p>I've glossed over four things.  I wonder if you can guess without reading on what they are?</p>

<p>.<br>
.<br>
.<br>
</p>

<p>My list of four things are: (1) What does "EXIT" do?  (2) which is related to (1) is how do
you call into a function, ie. how does %esi start off pointing at part of QUADRUPLE, but
then point at part of DOUBLE.  (3) What goes in the codeword for the words which are written
in FORTH?  (4) How do you compile a function which does anything except call other functions
ie. a function which contains a number like : DOUBLE 2 * ; ?</p>


</body>
</html>
