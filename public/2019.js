const DAYS_IN_MILLIS = 1 * 24 * 60 * 60 * 1000;
const START = new Date("2019-01-01").getTime(); // use UNIX Epoch instead
const DAYS_IN_THIS_YEAR = 365;

// TODO:
// Add header to the calendar, for month and for day of the week, copy gitlab maybe?

function create_table() {
    const pre_days = start_day_of_the_year();
    const proper_years = DAYS_IN_THIS_YEAR + pre_days; // the one that start at monday
    const cols = Math.ceil(proper_years / 7);
    const post_days = proper_years % 7;

    const template = document.querySelector("#template");
    const clone = document.importNode(template.content, true);
    const table = clone.querySelector("table");

    for (let i = 0; i < table.rows.length; i++) {
        const row = table.rows[i];
        for (let j = 0; j < cols; j++) {
            const cell = row.insertCell();
            if (is_outside(i, j, pre_days, post_days, cols)) {
                cell.classList.add("outside");
            }
        }
    }

    return table;
}

function is_outside(i, j, pre, post, cols) {
    return ((j == 0) && (i < pre)) || ((j == (cols-1)) && (i >= post));
}

function start_day_of_the_year() {
    const start_day = new Date(START).getDay(); // sunday = 0, ..., saturday = 6
    return (start_day + 6) % 7;
}

function num_to_date(n) {
    return new Date(START + n * DAYS_IN_MILLIS);
}

function yyyy_mm_dd_from_num(n) {
    return num_to_date(n).toISOString().slice(0, 10);
}

function day_and_date_from_num(n) {
    const date = num_to_date(n);
    const date_string = date.toISOString().slice(0, 10);
    const day = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"
    ][date.getDay()];
    return day + ", " + date_string;
}

function style_table(calendar, table) {
    let col = 0;
    let row = start_day_of_the_year();
    for (let i = 0; i < DAYS_IN_THIS_YEAR; i++) {
        const cell = table.rows[row].cells[col];

        row++;
        if (row == 7) {
            row = 0;
            col++;
        }

        cell.title = day_and_date_from_num(i);

        if (i >= calendar.length) {
            continue;
        }

        let cnum;
        if (calendar[i] >= 20) {
            cnum = 5;
        } else if (calendar[i] > 0) {
            cnum = Math.floor(calendar[i] / 5) + 1;
        } else {
            cnum = 0;
        }
        cell.classList.add("c" + cnum);

        let s;
        if (calendar[i] == 1) {
            s = "";
        } else {
            s = "s";
        }
        cell.title = "" + calendar[i] + " contribution" + s + "\n\n" + cell.title;
    }
}

function count_max_run(calendar) {
    let max_run = 0;
    let i = 0;
    while (i < calendar.length) {
        // skip consecutive zero
        while ((i < calendar.length) && (calendar[i] == 0)) {
            i++;
        }
        const start = i;
        // count the consecutive non-zero
        while ((i < calendar.length) && (calendar[i] != 0)) {
            i++;
        }
        max_run = Math.max(max_run, i - start);
    }
    return max_run;
}

function json_to_data(json) {
    const today = new Date();
    const ndays = Math.ceil((today - START) / DAYS_IN_MILLIS);

    let calendar = [];
    for (let i = 0; i < ndays; i++) {
        const key = yyyy_mm_dd_from_num(i);
        calendar[i] = json[key] || 0;
    }

    const percent = parseFloat(Math.round(ndays / 365 * 10000) / 100).toFixed(2); ;
    const max_run = count_max_run(calendar);
    return { "calendar": calendar, "ndays": ndays, "percent": percent, "max_run": max_run };
}

async function run() {
    let table = create_table();
    let main = document.getElementById("main");
    main.appendChild(table);
    let resp = await fetch("/resource/calendar.json");
    if (resp.ok) {
        let json = await resp.json();
        let data = json_to_data(json);

        style_table(data.calendar, table);

        const stats = document.createElement("p");
        stats.innerHTML = `* today is day ${data.ndays}<br>* longest streak is ${data.max_run} days<br>* ${data.percent}% of the year has been passed`;

        main.appendChild(stats);
    }
}

run();
